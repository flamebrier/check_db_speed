CREATE TABLE IF NOT EXISTS indications
(
    account     varchar(9),
    name        varchar(255),
    address     varchar(255),
    periodCode  varchar(4),
    sum         decimal,
    counters    varchar(255),
    indications decimal
    );