import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:enough_convert/windows/windows1251.dart';
import 'package:path_provider/path_provider.dart';

Future<String> get localPath async {
  final directory = await getDownloadsDirectory();

  return directory?.path ?? "";
}

Future<File> get localFile async {
  final path = await localPath;
  return File('$path/bd_data.utf8.txt');
}

Future<File> writeCounter(int counter) async {
  final file = await localFile;

  return file.writeAsString('$counter');
}

readCounter() async {
  String output = "";
  try {
    final file = await localFile;
    const codec = Windows1251Codec(allowInvalid: false);

    final Uint8List contents = await file.readAsBytes();
    try {
      for (int i = 0; i < contents.length; i += 1000) {
        final String? decoded = codec.decode(contents.getRange(i, i + 1000).toList());
        output += decoded ?? "";
        // log(decoded.toString());
      }
    } catch (e) {
      log(e.toString());
    }
  } catch (e) {
    return 0;
  }
}
