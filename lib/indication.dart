import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:check_db_speed/read_write.dart';
import 'package:enough_convert/windows/windows1251.dart';

Stream<Indication> readToStrings() async* {
  try {
    final file = await localFile;
    // const codec = Windows1251Codec(allowInvalid: false);
    var cnt = 0;
    await for (final line in file
        .openRead()
        .transform(utf8.decoder)
        .transform(const LineSplitter())) {
      final rc = recordStringToClass(line.trim());
      if (rc != null) {
        // log(rc.toString());
        cnt++;
        yield rc;
        // if (cnt > 500) {
        //   return;
        // }
      }
    }
  } catch (e) {
    log(e.toString());
  }

  // return strings;
}

Stream<String> readFileToStrings(File file) async* {
  try {
    final file = await localFile;
    await for (final line in file
        .openRead()
        .transform(utf8.decoder)
        .transform(const LineSplitter())) {
      yield line;
    }
  } catch (e) {
    log(e.toString());
  }

  // return strings;
}

readStringsToObjects(List<String> source) {
  final res = <Indication>[];
  for (final s in source) {
    log("строка $s");
    final tmp = recordStringToClass(s);
    log("объект $tmp");
    if (tmp != null) {
      res.add(tmp);
    }
  }
}

Indication? recordStringToClass(String record) {
  final tmp = record.split(";");
  if (tmp.isEmpty) {
    return null;
  }
  final ind = Indication();
  ind.accountF(tmp[0]);
  if (tmp.length < 2) return ind;
  ind.nameF(tmp[1]);
  if (tmp.length < 3) return ind;
  ind.addressF(tmp[2]);
  if (tmp.length < 4) return ind;
  ind.periodCodeF(tmp[3]);
  if (tmp.length < 5) return ind;
  ind.sumF(tmp[4]);
  if (tmp.length < 6) return ind;
  ind.countersF(tmp[5]);
  if (tmp.length < 7) return ind;
  ind.indicationsF(tmp[6]);
}

class Indication {
  String? account;
  String? periodCode;
  String? name;
  String? address;
  String? counters;
  double? sum;
  double? indications;

  Indication(
      /*{String? account,
      String? periodCode,
      String? name,
      String? address,
      String? counters,
      String? sum,
      String? indications}*/
      );

  accountF(String? source) {
    source = source?.trim() ?? "";
    if (source.length == 9) {
      account = source;
    }
  }

  periodCodeF(String? source) {
    source = source?.trim() ?? "";
    if (source.length == 4) {
      periodCode = source;
    } else {
      sumF(source);
    }
  }

  nameF(String? source) {
    source = source?.trim() ?? "";

    if (source.split(".").length == 3) {
      name = source;
    } else if (source.split(" ").length == 3) {
      name = source;
    } else {
      addressF(source);
    }
  }

  addressF(String? source) {
    source = source?.trim() ?? "";
    if (source.length > 20) {
      address = source;
    }
  }

  countersF(String? source) {
    source = source?.trim() ?? "";
    if (source.startsWith("33")) {
      counters = source;
    } else {
      indicationsF(source);
    }
  }

  sumF(String? source) {
    source = source?.trim() ?? "";
    double? res = double.tryParse(source);
    if (res != null) {
      sum = res;
    }
  }

  indicationsF(String? source) {
    source = source?.trim() ?? "";
    double? res = double.tryParse(source);
    if (res != null) {
      indications = res;
    }
  }

  @override
  String toString() {
    return "Счёт $account;\nФИО $name;\nАдрес $address;\nПериод $periodCode;\nСумма $sum;\nСчётчики $counters;\nПоказания $indications";
  }
}
