import 'dart:developer';
import 'dart:io';

import 'package:cassandart/cassandart.dart';
import 'package:flutter/cupertino.dart';

import 'indication.dart';

scyllaConnect(Stream<Indication> objs,
    {TextEditingController? outputController}) async {
  final client = await Cluster.connect(
    ['172.18.0.2:9042', '172.18.0.3:9042', '172.18.0.4:9042'],
    authenticator: PasswordAuthenticator('cassandra', 'cassandra'),
  );

  final fileCreate = File('doc/scylla/create_table.cql');
  for (var cmd in (await fileCreate.readAsString()).split(";")) {
    cmd = cmd.trim();
    if (cmd.isNotEmpty) {
      await client.execute(cmd);
    }
  }

  final fileInsert = File('doc/scylla/insert_values.cql');
  final insertCommand = await fileInsert.readAsString();
  // final fileInsert = File('doc/scylla/insertion.cql');
  // await for (final insertCommand in readFileToStrings(fileInsert)) {
  //   await client.execute(insertCommand);
  // }

  final startTime = DateTime.now();
  outputController?.text +=
      "\n${startTime.hour}:${startTime.minute}:${startTime.second}:${startTime.millisecond} Старт записи в Scylla";
  log("----Старт записи в Scylla $startTime");
  await for (final i in objs) {
    await client.execute(insertCommand, values: [
      i.account ?? "",
      i.name ?? "",
      i.address ?? "",
      i.periodCode ?? "",
      i.sum ?? 0.0,
      i.counters ?? "",
      i.indications ?? 0.0
    ]);
  }

  final endTime = DateTime.now();
  outputController?.text +=
      "\n${endTime.hour}:${endTime.minute}:${endTime.second}:${endTime.millisecond} Финиш записи в Scylla";
  outputController?.text +=
      "\nЗапись заняла в секундах: ${endTime.difference(startTime).inSeconds}";

  log("----Финиш через ${DateTime.now().difference(startTime)}");
}

Future<void> createInsertion(Stream<Indication> objs) async {
  final file = File('/home/flamebrier/Загрузки/insertion.cql').openWrite();

  await for (final i in objs) {
    file.write(
        'INSERT INTO benchmark.indications (account, name, address, period_code, sum, counters, indications) '
        'VALUES (\'${i.account ?? ""}\', \'${i.name ?? ""}\', \'${i.address ?? ""}\', \'${i.periodCode ?? ""}\', ${i.sum ?? 0}, \'${i.counters ?? ""}\', ${i.indications ?? 0});\n');
  }

  await file.flush();
  await file.close();
}
