import 'dart:developer';
import 'dart:io';

import 'package:check_db_speed/postgres.dart';
import 'package:check_db_speed/read_write.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'indication.dart';
import 'scylla.dart';

// docker run --network host --name pg --rm --env POSTGRES_HOST_AUTH_METHOD=trust postgres
// docker run --network host --name mongo --rm -e MONGO_INITDB_ROOT_USERNAME=mongoadmin -e MONGO_INITDB_ROOT_PASSWORD=secret mongo
// docker run --network host --name scylla --rm -it scylladb/scylla --smp 1 --memory 1G
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.amber,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final controller = TextEditingController(text: "Готово к работе");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Сравнение баз данных"),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(height: 15),
            Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(children: [
                    Container(height: 20),
                    Image.asset("assets/postgresql.png",
                        height: 250, isAntiAlias: true),
                  ]),
                  Column(
                    children: [
                      Image.asset("assets/scylla-logo.webp",
                          width: 130, isAntiAlias: true),
                      Image.asset("assets/scylla_text.png",
                          width: 130, isAntiAlias: true),
                    ],
                  ),
                ]),
            Container(height: 15),
            ElevatedButton(
                onPressed: () async {
                  final startTime = DateTime.now();
                  controller.text +=
                      "\n${startTime.hour}:${startTime.minute}:${startTime.second}:${startTime.millisecond} Старт чтения файла с базой в память...";
/*
                  final out = File("/home/flamebrier/Загрузки/bd_data.out.txt")
                      .openWrite();
                  final objs=<Indication>[];
                  await for (final obj in readToStrings()) {
                    objs.add(obj);
                    await out.flush();
                  }
                  await out.flush();
                  await out.close();
*/
                  final allObjs =
                      Stream.fromIterable(await readToStrings().toList());
                  final endTime = DateTime.now();
                  controller.text +=
                      "\n${endTime.hour}:${endTime.minute}:${endTime.second}:${endTime.millisecond} Конец чтения в память";
                  controller.text +=
                      "\nЧтение заняло в секундах: ${DateTime.now().difference(startTime).inSeconds}";
                  controller.text += "\n";

                  controller.text += "\n----------------";
                  controller.text += "\nПодготовка к работе с Postgres";
                  await postgresConnect(allObjs, outputController: controller);
                  controller.text += "\n----------------";
                  controller.text += "\n";
                  controller.text += "\n----------------";
                  controller.text += "\nПодготовка к работе со Scylla";
                  await scyllaConnect(allObjs, outputController: controller);
                  controller.text += "\n----------------";

                  // await createInsertion(allObjs);
                },
                child: const Text("Проверить базы данных")),
            Container(height: 15),
            Container(
              width: MediaQuery.of(context).size.width * 0.6,
              constraints: const BoxConstraints(minWidth: 300),
              clipBehavior: Clip.antiAlias,
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(21)),
              child: TextField(
                controller: controller,
                maxLines: 15,
                minLines: 5,
                readOnly: true,
                // enabled: false,
                decoration: const InputDecoration(
                    filled: true, fillColor: Colors.black12),
              ),
            )
          ],
        ),
      ),
    );
  }
}