import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:postgres/postgres.dart';

import 'indication.dart';

postgresConnect(Stream<Indication> objs,
    {TextEditingController? outputController}) async {
  var connection =
      PostgreSQLConnection("localhost", 5432, "postgres", username: "postgres");
  await connection.open();

  final fileCreate = File('doc/postgres/create_table_pg.sql');
  final createTableCommand = await fileCreate.readAsString();
  await connection.query(createTableCommand);

  final fileInsert = File('doc/postgres/insert_pg.sql');
  final insertCommand = await fileInsert.readAsString();

  int affectedRows = 0;

  final startTime = DateTime.now();
  outputController?.text +=
      "\n${startTime.hour}:${startTime.minute}:${startTime.second}:${startTime.millisecond} Старт записи в Postgres";
  log("----Старт записи в Postgres $startTime");
  await connection.transaction((connection) async {
    await for (final i in objs) {
      final res = await connection.query(insertCommand, substitutionValues: {
        "account": i.account ?? "",
        "name": i.name ?? "",
        "address": i.address ?? "",
        "periodCode": i.periodCode ?? "",
        "sum": i.sum ?? 0,
        "counters": i.counters ?? "",
        "indications": i.indications ?? 0
      });
      affectedRows += res.affectedRowCount;
    }
    return;
  });
  log("----Финиш через ${DateTime.now().difference(startTime)}");
  log("inserted $affectedRows");
  final endTime = DateTime.now();
  outputController?.text +=
      "\n${endTime.hour}:${endTime.minute}:${endTime.second}:${endTime.millisecond} Финиш записи в Scylla";
  outputController?.text +=
      "\nЗапись заняла в секундах: ${endTime.difference(startTime).inSeconds}";
  outputController?.text +=
      "\nВставлено строк: $affectedRows";
}
